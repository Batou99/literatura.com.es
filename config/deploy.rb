set :frontend, "46.4.119.85"
set :application, "literatura.com.es"

set :repo_server, "bitbucket.org"  
set :repo_user, "Batou99"
set :repository, "git@#{repo_server}:#{repo_user}/#{application}.git"
set :scm, :git
set :deploy_to, "/var/www/#{application}"
set :deploy_via, :remote_cache

role :web, frontend 
role :app, frontend
role :db,  frontend, :primary => true # This is where Rails migrations will run

set :user, "root"

namespace :deploy do
  task :start, :roles => :app do
    run "touch #{current_release}/tmp/restart.txt"
  end

  task :stop, :roles => :app do
    # Do nothing.
  end

  desc "Restart Application"
  task :restart, :roles => :app do
    run "touch #{current_release}/tmp/restart.txt"
    # run "nohup /etc/init.d/memcached restart &"
    run "ln -s /var/www/#{application}/shared/sphinx/ /var/www/#{application}/current/db/sphinx"
    run "chmod -R 777 /var/www/#{application}/shared/log/"
    run "chown -R www-data.www-data /var/www/#{application}/current"
  end
end

