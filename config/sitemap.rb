# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://literatura.com.es"

SitemapGenerator::Sitemap.add_links do |sitemap|
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: sitemap.add path, options
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  
  # Examples:
  letras = ('A'..'Z').to_a
  # Home
  
  # Paginas estaticas
  sitemap.add '/politica_de_privacidad'
  sitemap.add '/acerca_de'
  sitemap.add '/contacto'
  
  # Por libros
  #

  sitemap.add libros_path
  Libro.find_in_batches { |batch|  
    batch.each { |libro|  
      sitemap.add libro_path(libro)
    }
  }

  # Por Autores
  #

  sitemap.add autores_path
  Autor.valid.find_in_batches { |batch|  
    batch.each { |autor|  
      sitemap.add autor_path(autor)
    }
  }

  letras.each { |letra|  
    tp = (Autor.find_all_by_letra(letra.downcase).size/50).ceil
    (1..tp).each { |page|  
      sitemap.add "/autores/#{letra}-#{page}"
    }
  }

  # Por Editoriales
  #

  sitemap.add editoriales_path
  Editorial.find_in_batches { |batch|  
    batch.each { |editorial|  
      sitemap.add editorial_path(editorial)
    }
  }
  
  letras.each { |letra|  
    tp = (Editorial.find_all_by_letra(letra.downcase).size/50).ceil
    (1..tp).each { |page|  
      sitemap.add "/editoriales/#{letra}-#{page}"
    }
  }

  # Por Materias
  #
  
  sitemap.add materias_path
  Materia.find_in_batches {|batch|
    batch.each { |materia|  
      sitemap.add materia_path(materia)
    }
  }

  letras.each { |letra|  
    tp = (Materia.find_all_by_letra(letra.downcase).size/50).ceil
    (1..tp).each { |page|  
      sitemap.add "/materias/#{letra}-#{page}"
    }
  }

  # Por Epocas
  #
  sitemap.add "/epocas/"

  keys = (10..20).to_a
  values = %w( XI XII XIII XIV XV XVI XVII XVIII XIX XX XXI )
  siglos = {}
  keys.size.times { |i| siglos[ keys[i] ] = values[i] }
  siglos.keys.each {|key|
    siglo = siglos[key].to_i
    sitemap.add "/epocas/siglo/#{siglo}"
    inicio = siglo*100
    fin = (siglo+1)*100-1
    annos = (inicio..fin).to_a
    annos.delete_if {|anno| Libro.count(:conditions => ["anno = ?",anno])==0}
    annos.each {|anno|
      total = Libro.count(:conditions => ["anno = ?",anno])
      paginas = (total/50).ceil
      1.upto(total).to_a.each {|pag|
        sitemap.add "/epocas/siglo/#{siglo}/anno/#{anno.to_s}-#{pag.to_s}"
      }
    }
  }
end

