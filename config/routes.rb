# encoding: UTF-8
LiteraturaComEs::Application.routes.draw do

  get "materias/index"

  get "materias/show"

  get "editoriales/index"

  get "editoriales/show"

  resources :libros
  
  match "/autores/", :to => "autores#index"
  match "/autores/:letra_id-:page/", :constraints => {:letra_id => /[A-Z]/, :page => /[0-9]+/}, :to => "autores#index"
  
  resources :autores
  
  match "/editoriales/", :to => "editoriales#index"
  match "/editoriales/:letra_id-:page/", :constraints => {:letra_id => /[A-Z]/, :page => /[0-9]+/}, :to => "editoriales#index"
  match "/editoriales/:id-:page", :constraints => {:page => /[0-9]+/}, :to => "editoriales#show"

  resources :editoriales
  
  match "/materias/", :to => "materias#index"
  match "/materias/:letra_id-:page/", :constraints => {:letra_id => /[A-Z]/, :page => /[0-9]+/}, :to => "materias#index"
  match "/materias/:id-:page", :constraints => {:page => /[0-9]+/}, :to => "materias#show"
  
  resources :materias

  match "/busquedas/:busqueda_id/:tipo/:id" => "busquedas#show"
  
  resources :busquedas do
    resources :libros,:trailing_slash => true, :tipo=>'libros'
    resources :autores,:trailing_slash => true, :tipo=>'autores'
    resources :editoriales,:trailing_slash => true, :tipo=>'editoriales'
    resources :materias,:trailing_slash => true, :tipo=>'materias'
  end
  
  match "/epocas/", :to => "annos#index" 
  match "/epocas/siglo/:siglo", :to => "annos#index" #-:page", :constraints => {:siglo => /[XVI]+/, :page => /[0-9]+/}, :to => "annos#index"
  match "/epocas/siglo/:siglo/anno/:anno-:page", :to => "annos#show"

  match "/politica_de_privacidad.html", :to => 'base#politica_de_privacidad'
  match "/contacto.html", :to => 'base#contacto'
  match "/acerca_de.html", :to => 'base#acerca_de'

  root :to => 'libros#index'
end
