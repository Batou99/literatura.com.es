WillPaginate::ViewHelpers.pagination_options = {
    :class          => 'pagination',
    :previous_label => 'ANTERIOR',
    :next_label     => 'SIGUIENTE',
    :inner_window   => 4, # links around the current page
    :outer_window   => 2, # links around beginning and end
    :link_separator => '-', # single space is friendly to spiders and non-graphic browsers
    :param_name     => :page,
    :params         => nil,
    :renderer       => 'ApplicationHelper::RestLinkRenderer',
    :page_links     => true,
    :container      => true
  }
