class IndexOnNumeroDeLibros < ActiveRecord::Migration
  def self.up
    add_index :autores, :numero_de_libros
    add_index :editoriales, :numero_de_libros
  end

  def self.down
    remove_index :autores, :numero_de_libros
    remove_index :editoriales, :numero_de_libros
  end
end
