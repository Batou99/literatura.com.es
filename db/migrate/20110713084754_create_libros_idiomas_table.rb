class CreateLibrosIdiomasTable < ActiveRecord::Migration
  def self.up
    create_table :libros_idiomas, :force => true, :id => false do |t|
      t.integer :libro_id
      t.string :idioma_codigo
      t.string :idioma_rol
      t.timestamps
    end
    add_index :libros_idiomas, :libro_id
    add_index :libros_idiomas, :idioma_codigo
  end

  def self.down
    remove_index :libros_idiomas, :idioma_codigo
    remove_index :table_name,  :libro_id
    drop_table :libros_idiomas
  end
end