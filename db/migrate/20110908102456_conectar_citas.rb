class ConectarCitas < ActiveRecord::Migration
  def self.up
    Cita.all.each { |c|  
      autores = Autor.search c.nombre_autor
      if autores.size>0
        c.autor_id = autores[0].id
        c.save
      end
    }
  end

  def self.down
  end
end
