class DeleteAutorField < ActiveRecord::Migration
  def self.up
    remove_column :libros, :autor
  end

  def self.down
    add_column :libros, :autor, :string
  end
end