class CreateIdiomasTable < ActiveRecord::Migration
  def self.up
    create_table :idiomas, :force => true, :id => false do |t|
      t.string :codigo
      t.string :nombre
      t.string :es
      t.timestamps
    end
  end

  def self.down
    drop_table :idiomas
  end
end