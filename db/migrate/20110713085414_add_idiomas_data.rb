class AddIdiomasData < ActiveRecord::Migration
  
  def self.separar(_string)
    return [] unless _string!=nil
    _new_string=[]
    while _string.size>2
      _new_string << _string.slice!(0..2)
    end
    _new_string << _string if _string.size>0
    _new_string
  end
  
  def self.up
    LibrosIdioma.delete_all
    Libro.find_in_batches(:batch_size => 1000) do |libros|
      libros.each { |libro|  
        idiomas = AddIdiomasData.separar(libro.idioma)
        idiomas.each { |idioma|  
          LibrosIdioma.create(:libro_id => libro.id, :idioma_codigo => idioma, :idioma_rol => 'idioma')
        }
        
        idiomas = AddIdiomasData.separar(libro.idioma_original)
        idiomas.each { |idioma|  
          LibrosIdioma.create(:libro_id => libro.id, :idioma_codigo => idioma, :idioma_rol => 'idioma_original')
        }
      }
    end
    
  end

  def self.down
    LibrosIdioma.delete_all
  end
end
