class AddMateriasTable < ActiveRecord::Migration
  def self.up
    create_table :materias, :force => true do |t|
          t.string :nombre
          t.integer :numero_de_libros
          t.timestamps
    end
       
    add_index :materias, :nombre
    
    materias = Libro.all(:select => 'DISTINCT materia')
    materias.each { |mat|  
      m =Materia.find_or_create_by_nombre(mat.materia)
      m.save
    }
  end

  def self.down
    remove_index :materias, :nombre
    drop_table :materias
  end
end
