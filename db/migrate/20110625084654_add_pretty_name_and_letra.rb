class AddPrettyNameAndLetra < ActiveRecord::Migration
  def self.up
    add_column :libros, :pretty_name, :string
    add_column :libros, :letra, :char
    add_index :libros, :pretty_name
    add_index :libros, [:letra,:id]
    add_index :libros, :autor
    Libro.regenerar_todo
  end

  def self.down
    remove_index :libros, :autor
    remove_index :libros, [:letra,:id]
    remove_index :libros, :column => :pretty_name
    remove_column :libros, :letra
    remove_column :libros, :pretty_name
  end
end