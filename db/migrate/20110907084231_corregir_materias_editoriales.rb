class CorregirMateriasEditoriales < ActiveRecord::Migration
  def self.up
    Editorial.find_in_batches(:batch_size => 1000) {|editoriales|
      editoriales.each {|e|
        next unless e.nombre!=nil
        nombre = e.nombre.gsub(/^[^A-Za-z]*/,"")
        e.nombre = nombre
        e.letra = e.nombre[0..0].downcase
        e.save
      }
    }
    
    Materia.find_in_batches(:batch_size => 1000) {|materias|
      materias.each {|e|
        next unless e.nombre!=nil
        nombre = e.nombre.gsub(/^[^A-Za-z]*/,"")
        e.nombre = nombre
        e.letra = e.nombre[0..0].downcase
        e.save
      }
    }
    
    Libro.find_in_batches(:batch_size => 1000) {|libros|
      libros.each {|e|
        next unless e.editor!=nil
        nombre = e.editor.gsub(/^[^A-Za-z]*/,"")
        e.editor = nombre
        e.save
      }
    }
  end

  def self.down
  end
end
