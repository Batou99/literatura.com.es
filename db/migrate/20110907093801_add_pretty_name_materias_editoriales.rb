class AddPrettyNameMateriasEditoriales < ActiveRecord::Migration
  def self.up
    add_column :materias, :pretty_name, :string
    add_column :editoriales, :pretty_name, :string
    add_index :materias, :pretty_name
    add_index :editoriales, :pretty_name
    
    Materia.regenerar_pretty_names
    Editorial.regenerar_pretty_names
  end

  def self.down
    remove_index :editoriales, :pretty_name

    Materia.regenerar_pretty_names
    Editorial.regenerar_pretty_names
    remove_index :materias, :pretty_name
    remove_column :editoriales, :pretty_name
    remove_column :materias, :pretty_name
  end
end