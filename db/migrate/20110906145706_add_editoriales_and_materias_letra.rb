class AddEditorialesAndMateriasLetra < ActiveRecord::Migration
  def self.up
    add_column :editoriales, :letra, :string
    add_column :materias, :letra, :string
    add_index :editoriales, :letra
    add_index :materias, :letra

    Editorial.find_in_batches(:batch_size => 1000) {|editoriales|
      editoriales.each {|e|
        next unless e.nombre!=nil
        e.letra = e.nombre[0..0]
        e.save
      }
    }
    
    Materia.find_in_batches(:batch_size => 1000) {|materias|
      materias.each {|e|
        next unless e.nombre!=nil
        e.letra = e.nombre[0..0]
        e.save
      }
    }
    
  end

  def self.down
    remove_index :materias, :letra
    remove_index :editoriales, :letra
    remove_column :materias, :letra
    remove_column :editoriales, :letra
    
  end
end