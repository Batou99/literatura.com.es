class CreateAutoresTable < ActiveRecord::Migration
  def self.up
    create_table :autores, :force => true do |t|
      t.string :nombre
      t.integer :numero_de_libros
      t.integer :primer_libro_anno
      t.integer :ultimo_libro_anno
      t.string :letra
      t.string :fecha_de_nacimiento
      t.string :pretty_name
      t.timestamps
    end
    add_index :autores, :nombre
    add_index :autores, [:letra,:nombre]
    add_index :autores, :fecha_de_nacimiento
    add_index :autores, :pretty_name
  end

  def self.down
    remove_index :autores, :pretty_name
    remove_index :autores, :fecha_de_nacimiento
    remove_index :autores, :letra
    remove_index :autores, :nombre
    drop_table :autores
  end
end