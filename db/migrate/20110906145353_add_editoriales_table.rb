class AddEditorialesTable < ActiveRecord::Migration
  def self.up
    create_table :editoriales, :force => true do |t|
          t.string :nombre
          t.integer :numero_de_libros
          t.timestamps
    end
       
    add_index :editoriales, :nombre
    
    materias = Libro.all(:select => 'DISTINCT editor')
    materias.each { |mat|  
      m =Editorial.find_or_create_by_nombre(mat.editor)
      m.save
    }
  end

  def self.down
    remove_index :editoriales, :nombre
    drop_table :editoriales
  end
end
