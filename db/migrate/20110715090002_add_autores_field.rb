class AddAutoresField < ActiveRecord::Migration
  def self.up
    add_column :libros, :autor_id, :integer
    add_index :libros, :autor_id
    Libro.find_in_batches(:batch_size => 1000) do |libros|
      libros.each { |libro|
        begin
          if libro.autor!=nil
            a = Autor.find_by_nombre(libro.autor)
            libro.autor_id = a[:id]
            libro.save
          end
        rescue
          puts "Error en el libro #{libro.id}: con el titulo #{libro.titulo}"
        end
      }
    end
  end

  def self.down
    remove_index :libros, :autor_id
    remove_column :libros, :autor_id
  end
end