# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20111206104018) do

  create_table "annos", :force => true do |t|
    t.integer  "number"
    t.integer  "num_libros"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "autores", :force => true do |t|
    t.string   "nombre"
    t.integer  "numero_de_libros"
    t.integer  "primer_libro_anno"
    t.integer  "ultimo_libro_anno"
    t.string   "letra"
    t.string   "fecha_de_nacimiento"
    t.string   "pretty_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "autores", ["fecha_de_nacimiento"], :name => "index_autores_on_fecha_de_nacimiento"
  add_index "autores", ["letra", "nombre"], :name => "index_autores_on_letra_and_nombre"
  add_index "autores", ["nombre"], :name => "index_autores_on_nombre"
  add_index "autores", ["numero_de_libros"], :name => "index_autores_on_numero_de_libros"
  add_index "autores", ["pretty_name"], :name => "index_autores_on_pretty_name"

  create_table "citas", :force => true do |t|
    t.string   "nombre_autor"
    t.integer  "autor_id"
    t.string   "texto"
    t.boolean  "usada"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "citas", ["autor_id"], :name => "index_citas_on_autor_id"
  add_index "citas", ["nombre_autor"], :name => "index_citas_on_nombre_autor"
  add_index "citas", ["texto"], :name => "index_citas_on_texto"

  create_table "editoriales", :force => true do |t|
    t.string   "nombre"
    t.integer  "numero_de_libros"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "letra"
    t.string   "pretty_name"
  end

  add_index "editoriales", ["letra"], :name => "index_editoriales_on_letra"
  add_index "editoriales", ["nombre"], :name => "index_editoriales_on_nombre"
  add_index "editoriales", ["numero_de_libros"], :name => "index_editoriales_on_numero_de_libros"
  add_index "editoriales", ["pretty_name"], :name => "index_editoriales_on_pretty_name"

  create_table "idiomas", :id => false, :force => true do |t|
    t.string   "codigo"
    t.string   "nombre"
    t.string   "es"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "libros", :force => true do |t|
    t.integer  "anno"
    t.text     "raw"
    t.string   "titulo"
    t.string   "idioma"
    t.string   "idioma_original"
    t.string   "deposito_legal"
    t.string   "oficina_deposito_legal"
    t.string   "paginas"
    t.string   "tamano"
    t.string   "serie"
    t.string   "isbn"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "pretty_name"
    t.string   "letra",                   :limit => 1
    t.integer  "autor_id"
    t.string   "titulo_uniforme"
    t.string   "lugar_de_publicacion"
    t.string   "editor"
    t.string   "fecha_de_publicacion"
    t.string   "edicion"
    t.string   "isbn_foto"
    t.boolean  "has_photo"
    t.string   "subtitulo"
    t.string   "medio"
    t.string   "state_of_responsability"
    t.string   "materia"
  end

  add_index "libros", ["anno", "titulo"], :name => "index_libros_on_anno_and_titulo"
  add_index "libros", ["autor_id"], :name => "index_libros_on_autor_id"
  add_index "libros", ["deposito_legal"], :name => "index_libros_on_deposito_legal"
  add_index "libros", ["editor"], :name => "index_libros_on_editor"
  add_index "libros", ["fecha_de_publicacion"], :name => "index_libros_on_fecha_de_publicacion"
  add_index "libros", ["isbn"], :name => "index_libros_on_isbn"
  add_index "libros", ["letra", "id"], :name => "index_libros_on_letra_and_id"
  add_index "libros", ["materia"], :name => "index_libros_on_materia"
  add_index "libros", ["medio"], :name => "index_libros_on_medio"
  add_index "libros", ["pretty_name"], :name => "index_libros_on_pretty_name"
  add_index "libros", ["titulo"], :name => "index_libros_on_autor_and_titulo"
  add_index "libros", ["titulo_uniforme"], :name => "index_libros_on_titulo_uniforme"

  create_table "libros_idiomas", :id => false, :force => true do |t|
    t.integer  "libro_id"
    t.string   "idioma_codigo"
    t.string   "idioma_rol"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "libros_idiomas", ["idioma_codigo"], :name => "index_libros_idiomas_on_idioma_codigo"
  add_index "libros_idiomas", ["libro_id"], :name => "index_libros_idiomas_on_libro_id"

  create_table "materias", :force => true do |t|
    t.string   "nombre"
    t.integer  "numero_de_libros"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "letra"
    t.string   "pretty_name"
  end

  add_index "materias", ["letra"], :name => "index_materias_on_letra"
  add_index "materias", ["nombre"], :name => "index_materias_on_nombre"
  add_index "materias", ["pretty_name"], :name => "index_materias_on_pretty_name"

  create_table "recomendados", :force => true do |t|
    t.string   "titulo"
    t.integer  "libro_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "recomendados", ["libro_id"], :name => "index_recomendados_on_libro_id"
  add_index "recomendados", ["titulo"], :name => "index_recomendados_on_titulo"

  create_table "resultados_annos", :force => true do |t|
    t.integer  "anno"
    t.integer  "resultados"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
