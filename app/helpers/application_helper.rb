module ApplicationHelper
  class Array
    def shuffle!
      size.downto(1) { |n| push delete_at(rand(n)) }
      self
    end
  end
  
  class RestLinkRenderer < WillPaginate::ActionView::LinkRenderer

    def convertir_link(page,l)
      case @options[:controller]
      # when 'annos'
      #         l.gsub!(/\/\?page=[[:digit:]]+/,'\0/')
      #         l.gsub!(/\/\?page=/,'-')
      #       when 'personas'
      #         url = "/letra/#{@options[:letra_id]}-#{page}/#{@options[:rol].pluralize}/"  
      #         l.gsub!(/href=\".*\"/,"href='#{url}'")
      #       when 'universidades'
      #         _params = extraer_params(link)
      #         l.gsub!(/href=\".*?\"/,"href=\"/universidades/#{_params['universidad_id']}/documentos-#{_params['page']}/\"") if _params!=nil
      #       when 'descriptores'
      #         _params = extraer_params(link)
      #         l.gsub!(/href=\".*?\"/,"href=\"/descriptores/#{_params['descriptor_id']}/documentos-#{_params['page']}/\"") if _params!=nil
      #       when 'busquedas'
      #         l.gsub!(/\?page=[[:digit:]]+/,'\0/')
      #         l.gsub!(/[[:digit:]]+\?page=/,'')
      #       end
        when 'autores'
          url = "/autores/#{@options[:letra_id]}-#{page}/"  
          l.gsub!(/href=\".*\"/,"href='#{url}'")
        when 'busquedas'
          l.gsub!(/\?page=[[:digit:]]+/,'\0/')
          l.gsub!(/[[:digit:]]+\?page=/,'')
        when 'editoriales'
          url = "/editoriales/#{@options[:id]}-#{page}/"  
          l.gsub!(/href=\".*\"/,"href='#{url}'")  
        when 'materias'
          url = "/materias/#{@options[:id]}-#{page}/"  
          l.gsub!(/href=\".*\"/,"href='#{url}'")   
      end
      l
    end

    def previous_or_next_page(page, text, classname)
      if page
        l = link(text, page, :class => classname)
      else
        l = tag(:span, text, :class => classname + ' disabled')
      end
      convertir_link(page,l)
    end

    def page_number(page)
      unless page == current_page
        l = link(page, page, :rel => rel_value(page))
      else
        l= tag(:em, page)
      end
      convertir_link(page,l)

    end

    def extraer_params(_string)
      data = _string.match(/\?.*?"/)
      return nil if data==nil
      data = data.to_s[1..-1].split('&amp;')
      _params = {}

      data.each { |dato|  
        d = dato.split('=')
        _params[d[0]]=d[1].gsub(/\"/,'')
      }
      _params
    end
  
    def to_html
        html = pagination.map do |item|
          item.is_a?(Fixnum) ?
            page_number(item) :
            send(item)
        end
        html = html.first + html[1..-2].join(@options[:link_separator]) + html.last #Ponemos separador entre los numeros pero no entre "anterior" y "siguiente"
        @options[:container] ? html_container(html) : html
      end
  end
end
