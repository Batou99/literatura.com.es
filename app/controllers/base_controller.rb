# encoding: UTF-8
class BaseController < ApplicationController
  caches_action :politica_de_privacidad, :acerca_de, :contacto
  def politica_de_privacidad
    add_crumb "Política de privacidad"
    # Datos para el meta
    @meta = " Política de privacidad"
    @title = "tesis.com.es - Política de privacidad"
    @keywords = ", politica privacidad"
    respond_to do |format|
      format.html
    end
  end
  
  def acerca_de
    add_crumb "Acerca de"
    # Datos para el meta
    @meta = " Acerca de"
    @title = "tesis.com.es - Acerca de..."
    @keywords = ", acerca"
    respond_to do |format|
      format.html
    end
  end

  def contacto
    add_crumb "Contacto"
    # Datos para el meta
    @meta = " Contacto"
    @title = "tesis.com.es - Contacto"
    @keywords = ", contacto"
    respond_to do |format|
      format.html
    end
  end

end
