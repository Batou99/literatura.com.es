# encoding: UTF-8
class EditorialesController < ApplicationController

  caches_action :index, :cache_path => Proc.new { |controller| "editoriales:#{controller.params[:letra_id]}:#{controller.params[:page]}" }
  caches_action :show, :cache_path => Proc.new { |controller| "editoriales:#{controller.params[:id]}:#{controller.params[:page]}" } 
  def index
    if params[:letra_id] # Listado por letra
      
      add_crumb 'Editoriales', editoriales_url
      add_crumb params[:letra_id], "/editoriales/#{params[:letra_id]}-1/"
      add_crumb "Página #{params[:page]}"
      
      @editoriales = Editorial.paginate :page => params[:page], :order => 'nombre', :conditions => ['letra=?', params[:letra_id]]
      
      sql = "SELECT DISTINCT letra FROM editoriales"
      letras = ActiveRecord::Base.connection.execute(sql)
      @letras = []
      letras.each { |e| @letras << e[0] if e[0]!=nil && e[0].match(/[A-Za-z]/)!=nil}
    
    else # Listado general de autores
      
      add_crumb 'Editoriales'
    
      sql = "SELECT DISTINCT letra FROM editoriales"
      letras = ActiveRecord::Base.connection.execute(sql)
      @letras = {}
    
      letras.each { |e|
        next unless e[0]!=nil && e[0].match(/[A-Za-z]/)!=nil
        primero = Editorial.find(:first, :conditions => ['letra=?',e[0]], :order => 'nombre').nombre.capitalize.split(',')[0][0..10]
        ultimo = Editorial.find(:last, :conditions => ['letra=?',e[0]], :order => 'nombre').nombre.capitalize.split(',')[0][0..10]

        @letras[e[0]] = [primero,ultimo]
      }
      
      keys = @letras.keys.sort
      
      @letrasA = @letras.select {|k,v| keys[0..10].include?(k)}
      @letrasB = @letras.select {|k,v| keys[11..21].include?(k)}
      @letrasC = @letras.select {|k,v| keys[22..-1].include?(k)}
      # @letrasB = @letras[(@letras.size/3+1)..(2*@letras.size/3)]
      # @letrasC = @letras[((2*@letras.size/3)+1)..-1]
      #@letras['Otros'] = 
    end
 
  end
  
  def show
    @editorial = Editorial.find_by_pretty_name(params[:id])
    libros = Libro.find_all_by_editor(@editorial.nombre) #, :order => 'coalesce(titulo_uniforme,titulo),anno,CASE idioma WHEN "spa" THEN 0 ELSE 1 END')
    # Si utilizamos coalesce para ordenar no usa el indice
    lbs = {}
    libros.each {|l|
      key = "#{l.titulo_uniforme!},#{l.anno},#{l.idioma=='spa'?0:1}"
      lbs[key] = l
    }
    libros = lbs.sort.map {|key,value| value}
    
    add_crumb 'Editoriales', editoriales_url
    add_crumb @editorial.pretty_name,"/editoriales/#{@editorial.nombre}-1"
    add_crumb "Página #{params[:page]}"
    
    @titulos = {}

    libros.each { |libro| 
      next unless @titulos[libro.titulo_uniforme!]==nil 
      @titulos[libro.titulo_uniforme!] = libro
    }

    ids = []
    @titulos.keys.each { |k|  
      ids << @titulos[k].id
    }
    
    @libros = Libro.by_ids(ids).page(params[:page]).order('titulo')

  end

end
