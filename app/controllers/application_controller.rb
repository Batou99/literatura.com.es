class ApplicationController < ActionController::Base
  
  #rescue_from NoMethodError, :with => :redireccion_al_buscador
  
  protect_from_forgery
  add_crumb "Home", '/'
  
  def redireccion_al_buscador(exception)
    if params[:id]==nil
      keys = params.keys.select {|x| x=~/id/}
      params[:id] = params[keys[0]] ||= params[params.keys[0]] ||= "Error"
    end
    redirect_to :controller => 'busquedas', :action => 'show', :id => params[:id].gsub(/-/,' '), :tipo => 'error'
  end
  
end
