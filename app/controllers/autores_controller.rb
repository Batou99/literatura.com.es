# encoding: UTF-8
class AutoresController < ApplicationController
  caches_action :index, :cache_path => Proc.new { |controller| "autores:#{controller.params[:letra_id]}:#{controller.params[:page]}" } 
  caches_action :show
  def index

    if params[:letra_id] # Listado por letra
      
      add_crumb 'Autores', autores_url
      add_crumb params[:letra_id], "/autores/#{params[:letra_id]}-1/"
      add_crumb "Página #{params[:page]}"
      
      @autores = Autor.paginate :page => params[:page], :order => 'nombre', :conditions => ['letra=?', params[:letra_id]]
      
      sql = "SELECT DISTINCT letra FROM autores"
      letras = ActiveRecord::Base.connection.execute(sql)
      @letras = []
      letras.each { |e| @letras << e[0] if e[0]!=nil && e[0].match(/[A-Za-z]/)!=nil}
    
    else # Listado general de autores
      
      add_crumb 'Autores'
    
      sql = "SELECT DISTINCT letra FROM autores"
      letras = ActiveRecord::Base.connection.execute(sql)
      @letras = {}
    
      letras.each { |e|
        next unless e[0]!=nil && e[0].match(/[A-Za-z]/)!=nil
        primero = Autor.find(:first, :conditions => ['letra=?',e[0]], :order => 'nombre').nombre.capitalize.split(',')[0][0..10]
        ultimo = Autor.find(:last, :conditions => ['letra=?',e[0]], :order => 'nombre').nombre.capitalize.split(',')[0][0..10]

        @letras[e[0]] = [primero,ultimo]
      }
      
      keys = @letras.keys.sort
      
      @letrasA = @letras.select {|k,v| keys[0..10].include?(k)}
      @letrasB = @letras.select {|k,v| keys[11..21].include?(k)}
      @letrasC = @letras.select {|k,v| keys[22..-1].include?(k)}
      # @letrasB = @letras[(@letras.size/3+1)..(2*@letras.size/3)]
      # @letrasC = @letras[((2*@letras.size/3)+1)..-1]
      #@letras['Otros'] = 
    end
    
  end

  def show
    @autor = Autor.find_by_pretty_name(params[:id])
    add_crumb 'Autores', autores_url
    add_crumb @autor.nombre
    
    libros = Libro.find_all_by_autor_id(@autor) #, :order => 'coalesce(titulo_uniforme,titulo),anno,CASE idioma WHEN "spa" THEN 0 ELSE 1 END')
    # Si utilizamos coalesce para ordenar no usa el indice
    lbs = {}
    libros.each {|l|
      key = "#{l.titulo_uniforme!},#{l.anno},#{l.idioma=='spa'?0:1}"
      lbs[key] = l
    }
    # @annos = {}
    #     libros.each { |libro|  
    #       @annos[libro.anno] = [] unless @annos[libro.anno] != nil
    #       @annos[libro.anno] << libro
    #     }
    @titulos = {}
    libros.each { |libro|  
      @titulos[libro.titulo_uniforme!] = [] unless @titulos[libro.titulo_uniforme!] != nil
      @titulos[libro.titulo_uniforme!] << libro
    }
    
    @annos = {}
    @titulos.keys.each { |key|
      @annos[@titulos[key][0].anno] = [] unless @annos[@titulos[key][0].anno] != nil
      @annos[@titulos[key][0].anno] << key
    }
  end

end
