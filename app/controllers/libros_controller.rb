class LibrosController < ApplicationController
  caches_action :show  
  
  def index
    @semana_del = (1.week.ago.end_of_week + 1.day).strftime("%d/%m/%Y")
    @recomendados = Recomendado.recomendados_de_la_semana 
    @autores_prolificos = Autor.find(:all,:order => 'numero_de_libros DESC',:limit => 10)
    @editoriales_prolificas = Editorial.find(:all, :order => 'numero_de_libros DESC', :limit => 10)
  end

  def show
    @libro = Libro.find_by_pretty_name(params[:id]) 
    @otros = @libro.autor.otros_libros(@libro).shuffle![0..3] if @libro.autor
   
    add_crumb "Libros", '/libros'
    if @libro.autor
      add_crumb @libro.autor.nombre, autor_url(@libro.autor)
    else
      add_crumb 'Autor Desconocido',"#"
    end
    add_crumb @libro.titulo_corto
    
    @libro.borrow_image if @libro.isbn_foto==nil and @libro.has_photo!=0
    
  end

end
