# encoding: UTF-8
class AnnosController < ApplicationController

  caches_action :index, :cache_path => Proc.new { |controller| "annos:#{controller.params[:siglo]}" }
  caches_action :show, :cache_path => Proc.new { |controller| "annos:#{controller.params[:siglo]}:#{controller.params[:anno]}" } 
  def index

    keys = (10..20).to_a
    values = %w( XI XII XIII XIV XV XVI XVII XVIII XIX XX XXI )
    @siglos = {}
    keys.size.times { |i| @siglos[ keys[i] ] = values[i] }

    if params[:siglo] # Vista por siglo
      add_crumb 'Siglos','/epocas'
      add_crumb params[:siglo] #,"/annos/siglos/#{params[:siglo]}-1"
      siglo_num =  @siglos.invert[params[:siglo]]
      inicio = siglo_num*100
      fin = (siglo_num+1)*100-1

      @annos = (inicio..fin).to_a
      @annos.delete_if {|anno| Libro.count(:conditions => ["anno = ?",anno])==0}
      size = @annos.size
      @annosA = @annos[0..size/3]
      @annosB = @annos[(size/3)+1..(2*size/3)]
      @annosC = @annos[((2*size/3)+1)..-1]
      
      @annosA = [] if @annosA == nil 
      @annosB = [] if @annosB == nil 
      @annosC = [] if @annosC == nil 

      

      @siglosA = @siglos.select {|k,v| keys[0..4].include?(k)}
      @siglosB = @siglos.select {|k,v| keys[5..8].include?(k)}
      @siglosC = @siglos.select {|k,v| keys[9..10].include?(k)}

      @siglosA.delete(siglo_num)
      @siglosB.delete(siglo_num)
      @siglosC.delete(siglo_num)

    else # Vista general de los siglos
      add_crumb 'Siglos'  
      @siglosA = @siglos.select {|k,v| keys[0..3].include?(k)}
      @siglosB = @siglos.select {|k,v| keys[4..7].include?(k)}
      @siglosC = @siglos.select {|k,v| keys[8..10].include?(k)}


    end
  end

  def show
    add_crumb 'Siglos', '/epocas/'
    add_crumb params[:siglo], "/epocas/siglo/#{params[:siglo]}"
    add_crumb params[:anno], "/epocas/siglo/#{params[:siglo]}/anno/#{params[:anno]}-1"
    add_crumb "Pagina #{params[:page]}"

    @libros = Libro.paginate :page => params[:page], :order => 'titulo', :conditions => ['anno=?',params[:anno]]
  end

end
