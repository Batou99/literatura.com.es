# encoding: UTF-8
class MateriasController < ApplicationController
  caches_action :index, :cache_path => Proc.new { |controller| "materias:#{controller.params[:letra_id]}:#{controller.params[:page]}" }
  caches_action :show, :cache_path => Proc.new { |controller| "materias:#{controller.params[:id]}:#{controller.params[:page]}" } 
  def index
    if params[:letra_id] # Listado por letra
      
      add_crumb 'Materias', materias_url
      add_crumb params[:letra_id], "/materias/#{params[:letra_id]}-1/"
      add_crumb "Página #{params[:page]}"
      
      @materias = Materia.paginate :page => params[:page], :order => 'nombre', :conditions => ['letra=?', params[:letra_id]]
      
      sql = "SELECT DISTINCT letra FROM materias"
      letras = ActiveRecord::Base.connection.execute(sql)
      @letras = []
      letras.each { |e| @letras << e[0] if e[0]!=nil && e[0].match(/[A-Za-z]/)!=nil}
    
    else # Listado general de autores
      
      add_crumb 'Materias'
    
      sql = "SELECT DISTINCT letra FROM materias"
      letras = ActiveRecord::Base.connection.execute(sql)
      @letras = {}
    
      letras.each { |e|
        next unless e[0]!=nil && e[0].match(/[A-Za-z]/)!=nil
        primero = Materia.find(:first, :conditions => ['letra=?',e[0]], :order => 'nombre').nombre.capitalize.split(',')[0][0..10]
        ultimo = Materia.find(:last, :conditions => ['letra=?',e[0]], :order => 'nombre').nombre.capitalize.split(',')[0][0..10]

        @letras[e[0]] = [primero,ultimo]
      }
      
      keys = @letras.keys.sort
      
      @letrasA = @letras.select {|k,v| keys[0..10].include?(k)}
      @letrasB = @letras.select {|k,v| keys[11..21].include?(k)}
      @letrasC = @letras.select {|k,v| keys[22..-1].include?(k)}
      # @letrasB = @letras[(@letras.size/3+1)..(2*@letras.size/3)]
      # @letrasC = @letras[((2*@letras.size/3)+1)..-1]
      #@letras['Otros'] = 
    end
  end

  def show
    @materia = Materia.find_by_pretty_name(params[:id])
    libros = Libro.find_all_by_materia(@materia.nombre) #, :order => 'coalesce(titulo_uniforme,titulo),anno,CASE idioma WHEN "spa" THEN 0 ELSE 1 END')

    # Si utilizamos coalesce para ordenar no usa el indice
    lbs = {}
    libros.each {|l|
      key = "#{l.titulo_uniforme!},#{l.anno},#{l.idioma=='spa'?0:1}"
      lbs[key] = l
    }


    add_crumb 'Materias', materias_url
    add_crumb @materia.nombre,"/materias/#{@materia.nombre}-1"
    add_crumb "Página #{params[:page]}"
    
    # @annos = {}
    #     libros.each { |libro|  
    #       @annos[libro.anno] = [] unless @annos[libro.anno] != nil
    #       @annos[libro.anno] << libro
    #     }
    @titulos = {}
    libros.each { |libro| 
      next unless @titulos[libro.titulo_uniforme!]==nil 
      @titulos[libro.titulo_uniforme!] = libro
    }

    ids = []
    @titulos.keys.each { |k|  
      ids << @titulos[k].id
    }
    
    @libros = Libro.by_ids(ids).page(params[:page]).order('titulo')

  end

end
