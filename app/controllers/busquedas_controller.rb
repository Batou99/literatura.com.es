# encoding: UTF-8
class BusquedasController < ApplicationController
  def show
    add_crumb "Búsquedas",'#'
    
    if params[:busqueda_id]
      @busqueda = Busqueda.new(params[:busqueda_id],params[:tipo],params[:id])
      
      add_crumb params[:busqueda_id],'#'
      add_crumb "Página #{params[:id]}"
      
      # @keywords = "tesis búsquedas #{params[:busqueda_id]}"
      # @meta = "Búsqueda por #{params[:tipo]} - #{params[:busqueda_id]}"
      # @title = "#{@meta} - Tesis - tesis.com.es"
    else
      @busqueda = Busqueda.new(params[:id])
      
      add_crumb params[:id]
      
      
      # @keywords = "tesis búsquedas #{params[:id]}"
      # @meta = "Búsqueda: #{params[:id]}"
      # @title = "#{@meta} - Tesis - tesis.com.es"
    end

    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.xml  { render :xml => @busqueda }
    # end
  end
  
  def create
    params[:busqueda]='Internet para torpes' if params[:busqueda]=="" 
    @busqueda = Busqueda.new(params[:busqueda])
    
    #add_crumb "Búsquedas"
    #add_crumb params[:busqueda][:texto]
    
    
    # @keywords = "tesis búsquedas #{params[:busqueda][:texto]}"
    # @meta = "Búsqueda: #{params[:busqueda][:texto]}"
    # @title = "#{@meta} - Tesis - tesis.com.es"
    
    redirect_to :action => :show, :id => params[:busqueda]
  end

end
