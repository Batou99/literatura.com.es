class Materia < ActiveRecord::Base
  include PrettyNames::InstanceMethods
  extend PrettyNames::ClassMethods

  before_save :arreglar_pretty_name

  define_index do
    indexes nombre, :sortable => true
  end

end
