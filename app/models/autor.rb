# encoding: UTF-8
require 'enumerator'
class Autor < ActiveRecord::Base
  include PrettyNames::InstanceMethods
  extend PrettyNames::ClassMethods

  scope :valid, lambda { where('nombre<>"" and nombre is not null') }

  before_save :arreglar_pretty_name
  has_many :libros
  has_many :citas
  
  cattr_reader :per_page
  @@per_page = 50
    
  define_index do
    indexes nombre, :sortable => true
    indexes fecha_de_nacimiento
  end
 
  def otros_libros(_libro)
    lista = {}
    libros.each {|libro|
      lista[libro.titulo_uniforme] = libro if libro.titulo_uniforme!!=_libro.titulo_uniforme!
    }
    lista.values
  end
  

  def self.recalcular_numero_de_libros
    sql = "SELECT COUNT(DISTINCT COALESCE(titulo_uniforme,titulo)),autor_id FROM libros GROUP BY autor_id ORDER BY  COUNT(DISTINCT COALESCE(titulo_uniforme,titulo)) DESC limit 100"
    resultados =  ActiveRecord::Base.connection.execute(sql)
    #resultados.in_groups_of(1000) { |group|
      resultados.each { |resultado|  
      begin
        a = Autor.find(resultado[1])
        a.numero_de_libros = resultado[0]
        a.save
      rescue Exception => ex
        puts ex
      end
      }
    #}
  end
    

end
