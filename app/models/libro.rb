# encoding: UTF-8
require 'asin'
class Libro < ActiveRecord::Base
  include PrettyNames::InstanceMethods
  extend PrettyNames::ClassMethods

  before_save :arreglar_pretty_name
  has_many :libros_idiomas
  has_many :idiomas, :through => :libros_idiomas, :conditions => ["libros_idiomas.idioma_rol = ?", "idioma"], :source => 'idioma' 
  has_many :idiomas_originales, :through => :libros_idiomas, :conditions => ["libros_idiomas.idioma_rol = ?", "idioma_original"], :foreign_key => 'idioma_original', :source => 'idioma' 

  belongs_to :autor

  define_index do
    indexes titulo, :sortable => true
    indexes :anno, :sortable => true
    indexes deposito_legal, oficina_deposito_legal, serie, isbn
  end

  def self.by_ids(ids)
    where("id in (#{ids.join(',')})")
  end

  def nombre
    serie == nil ? titulo : "#{titulo} / #{serie}"
  end

  def titulo_corto
    chars = titulo.size
    if chars>40 
      return "#{titulo[0..50]}..."
    else
      return titulo
    end
  end
  
  def idioma_t
    idiomas.collect{|x| x.name('es')}.join('/')
  end
  
  def idioma_original_t
    idiomas_originales.collect{|x| x.name('es')}.join('/')
  end
  
  def titulo_uniforme!
    self.titulo_uniforme ||= self.titulo
  end
  
  def check_image
    if self.isbn_foto==nil && self.has_photo!=0
      client = ASIN::Client.instance
      client.configure :secret => 'VxTmNJsLaB5xTl0xP3Tau98XF4tsYRcbRTpacGeg', :key => '0HD5AS1SFXZAA7EF8CG2'
      # lookup an item with the amazon standard identification number (asin)
      
      item = client.lookup self.isbn.gsub(/-/,'')
      if item.title!=nil
        self.isbn_foto=self.isbn.gsub(/-/,'')
        self.has_photo = true
        self.save
        return
      end
    end
  end
  
  def check_others
    begin
      libros = Libro.find_all_by_titulo_uniforme(titulo_uniforme ||= titulo)
      libros.delete(self)
      for libro in libros
        libro.check_image 
      end
    rescue
    end
  end
  
  def borrow_image
    begin
      check_image
      if self.isbn_foto==nil && self.has_photo!=0
        self.titulo_uniforme ? libros = Libro.find_all_by_titulo_uniforme(self.titulo_uniforme) : libros = Libro.find_all_by_titulo(self.titulo)
        libros.delete(self)
        for libro in libros
          libro.check_image
          if libro.isbn_foto!=nil
            self.isbn_foto=libro.isbn_foto
            self.has_photo = true
           # break
          end
        end
        if self.isbn_foto==nil 
          self.has_photo=false
        end
        self.save
      end
    rescue
    end
  end
  
end
