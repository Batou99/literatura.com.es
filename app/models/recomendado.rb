class Recomendado < ActiveRecord::Base
  belongs_to :libro

  def self.recomendados_de_la_semana
    semana = Date.today.cweek
    rstart =  (Date.today.year-2011)*53 + (Date.today.cweek-1)*4
    rend =  (Date.today.year-2011)*53 + Date.today.cweek*4 - 1
    Recomendado.find(:all, :conditions => "libro_id is not null")[rstart..rend]
  end
end
