# encoding: UTF-8
class Idioma < ActiveRecord::Base
  
  set_primary_key :codigo
  
  def name(_code)
    send(_code)==nil ? nombre : send(_code)
  end
  
end