# encoding: UTF-8
module PrettyNames

  module InstanceMethods

    def arreglar_pretty_name
      pretty_name='__NULL__' if pretty_name==nil
      if pretty_name!=nil 
        dup = self.class.find_all_by_pretty_name(pretty_name)
        dup.delete(self)
        return unless dup!=[]
      end
      
      
      dup = self.class.find_all_by_pretty_name(nombre_limpio)
      dup = [] unless dup!=nil
      dup.delete(self)
      if dup==[]
        self.pretty_name=nombre_limpio
        return
      end 
      
      culprits = self.class.find(:all, :conditions => ['pretty_name like ?',"#{nombre_limpio}\\_%"])
      culprits = [] unless culprits!=nil
      culprits.delete(self)
      if culprits==[]
        self.pretty_name = "#{nombre_limpio}_1"
      else
        culprits.sort! { |x,y| x.pretty_name.split('_')[1].to_i <=> y.pretty_name.split('_')[1].to_i }
        num = culprits[-1].pretty_name.match(/_[[:digit:]]+$/)
        self.pretty_name = "#{nombre_limpio}_#{num.to_s[1..-1].to_i+1}"
      end
      true
    end

    def arreglar_letra
      self.letra = nombre[0..0].downcase
    end

    def to_param
      self.pretty_name
    end

    def nombre_limpio # Quita ñ, acentos, etc
      
      palabras_vacias = %w{u e o y el los la las lo al del un unos una unas a ante bajo cabe con contra de desde durante en entre hacia hasta mediante para por segun sin so sobre tras	via}
      
      _string = String.new(nombre)
      _string.downcase!
      _string.gsub!(/[áÁ]+/i,'a')
      _string.gsub!(/[éÉ]+/i,'e')
      _string.gsub!(/[íÍ]+/i,'i')
      _string.gsub!(/[óÓ]+/i,'o')
      _string.gsub!(/[úÚ]+/i,'u')
      _string.gsub!(/[Ññ]+/i,'n')
      _string.gsub!(/[^a-z\s 0-9]+/i,'')
      _string.gsub!(/\s/,'-')
      palabras_vacias.each { |palabra|  
        _string.gsub!(Regexp.new("-#{palabra}-"),'-')
        _string.gsub!(Regexp.new("^#{palabra}-"),'')
        _string.gsub!(Regexp.new("-#{palabra}$"),'')
      }
      _string.gsub!(/-$/,'')
      if (_string.size>80) 
        pos = _string.index('-',-30)
        _string = _string[0..(pos-1)] if pos!=nil
      end
      _string
    end
  end
  
  module ClassMethods
    def find_by_pretty_name(_name)
      # Rails.cache.fetch("#{self.class.name}:#{_name}") { 
        self.find(:first,:conditions=>['pretty_name=?',_name])
      # }
    end

    def regenerar_pretty_names
      sql = ActiveRecord::Base.connection();
      sql.update("UPDATE #{self.name.pluralize} SET pretty_name=NULL")
      self.find_in_batches do |objetos|
        objetos.each { |ob|  
          ob.pretty_name = ""
          ob.save
        }
      end
    end
    
    # def recalcular_totales
    #   objetos = self.find(:all)
    #   sql = ActiveRecord::Base.connection()
    #   objetos.each { |ob|  
    #     # next unless ob.numero_de_tesis==nil
    #     numero_de_tesis = ob.documentos.count
    #     # No queremos activar before_save
    #     sql.update("UPDATE #{self.name.pluralize} SET numero_de_tesis=#{numero_de_tesis} WHERE id=#{ob.id}")
    #   }
    # end
    
    def regenerar_todo
      # i=0
      objetos = self.find_in_batches(:batch_size => 1000) do |obs|
        obs.each { |ob|  
          begin
            ob.letra = ob.nombre[0..0].downcase
            ob.pretty_name = ""
            ob.save
          rescue
            puts "Error en id #{ob.id}"
          end
        }
        # i=i+1
        # puts "Grupo #{i} acabado"
      end
    end
  end
  
end
