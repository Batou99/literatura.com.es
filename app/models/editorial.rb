class Editorial < ActiveRecord::Base
  include PrettyNames::InstanceMethods
  extend PrettyNames::ClassMethods

  before_save :arreglar_pretty_name

  define_index do
    indexes nombre, :sortable => true
  end

  def self.recalcular_numero_de_libros
    sql = "SELECT COUNT(DISTINCT COALESCE(titulo_uniforme,titulo)), editor FROM libros GROUP BY editor ORDER BY  COUNT(DISTINCT COALESCE(titulo_uniforme,titulo)) DESC limit 500"
    resultados =  ActiveRecord::Base.connection.execute(sql)
    #resultados.in_groups_of(1000) { |group|
      resultados.each { |resultado|  
      begin
        a = Editorial.find_by_nombre(resultado[1])
        a.numero_de_libros = resultado[0]
        a.save
      rescue Exception => ex
        puts ex
      end
      }
    #}
  end
end

