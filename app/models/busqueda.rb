class Busqueda
  attr :texto
  attr_reader :libros
  attr_reader :autores
  attr_reader :editoriales
  attr_reader :materias
  attr_reader :data
  
  def to_param
    texto
  end
  
  def initialize(_texto=nil,_tipo=nil,_page=1)
    @libros = []
    @autores = []
    @editoriales = []
    @materias = []
    @texto = _texto
    return unless _tipo || _texto
    case _tipo # Mostramos la primera pagina de todos
      when nil
        # Vamos a mostrar 20 resultados
        @autores = Autor.search _texto, :order => :nombre, :per_page => 5
        @editoriales = Editorial.search _texto, :order => :nombre, :per_page => 5
        @materias = Materia.search _texto, :order => :nombre, :per_page => 5
        @libros = Libro.search _texto, :order => :titulo, :per_page => (20-@autores.size-@editoriales.size-@materias.size)
      when 'libros'
        @data = Libro.search _texto, :per_page => 20, :page => _page
      else 
        @data = eval("#{_tipo.singularize.capitalize}.search '#{_texto}', :order => :nombre, :per_page => 20, :page => #{_page}")
      end
  end
end
