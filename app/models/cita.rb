class Cita < ActiveRecord::Base
  belongs_to :autor  

  def self.cita_de_la_semana
    num = (Date.today.year-2011)*53 + Date.today.cweek
    Cita.find(:all,:conditions => 'autor_id is not null')[num % self.size]
  end

  def self.cita_del_dia
    anno = Date.today.year
    dia = (Date.today-Date.new(anno,1,1)).to_i
    num = (anno-2011)*366+dia
    Cita.find(:all,:conditions => 'autor_id is not null')[num % self.size]
  end

  private

  def self.size
    Cita.count(:conditions => 'autor_id is not null')
  end

end
